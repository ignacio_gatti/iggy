﻿using System;
using System.IO;
using SQLite;
using Xamarin.Forms;
using SetupSQLite.iOS;

[assembly: Dependency(typeof(SQLiteDb))]

namespace SetupSQLite.iOS
{
    public class SQLiteDb : ISQLiteDb
    {
        public SQLiteAsyncConnection GetConnection()
        {
			var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); 
            var path = Path.Combine(documentsPath, "IgnacioGatti.db3");

            return new SQLiteAsyncConnection(path);
        }
    }
}

