﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public class WebURL
	{
        [SQLite.PrimaryKey, SQLite.AutoIncrement]
        public int ID { get; set; }
        public string URL { get; set; }
        public string ImageShow { get; set; }
        public string Title { get; set; }
    }

	public partial class ASSN3Page : ContentPage
	{
        private SQLiteAsyncConnection _connection;
        private List<WebURL> _urls;
        public ASSN3Page()
		{
			InitializeComponent();
            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();

			var tap1 = new TapGestureRecognizer();
			var tap2 = new TapGestureRecognizer();
			var tap3 = new TapGestureRecognizer();
			var tap4 = new TapGestureRecognizer();

			tap1.Tapped += OpenWebView;
			tap2.Tapped += AddURLAction;
			tap3.Tapped += UpdateURLAction;
			tap4.Tapped += RemoveURL;


			GoToLink.GestureRecognizers.Add(tap1);
			AddURL.GestureRecognizers.Add(tap2);
			EditURL.GestureRecognizers.Add(tap3);
			DeleteLink.GestureRecognizers.Add(tap4);

        }

		async void ReloadPicker()
		{
            await _connection.CreateTableAsync<WebURL>();
            _urls = await _connection.Table<WebURL>().ToListAsync();

            if (ShowURLs.Items.Count != 0)
			{
                ShowURLs.Items.Clear();
			}

			foreach (var x in _urls)
			{
                ShowURLs.Items.Add($"{x.Title}");
			}
		}

		protected override void OnAppearing()
		{
			ReloadPicker();

			base.OnAppearing();
		}

		async void OpenWebView(object sender, System.EventArgs e)
		{
			if (ShowURLs.SelectedIndex == -1)

			{
				await DisplayAlert("Error", "You Need to Pick A Link", "Ok");
			}

			else
			{
				var passOnUrl = _urls[ShowURLs.SelectedIndex].URL;
				await Navigation.PushModalAsync(new WebViewShow(passOnUrl));
			}

		}

		async void AddURLAction(object sender, System.EventArgs e)
		{

			await Navigation.PushModalAsync(new AddURLPage());
		}

		async void UpdateURLAction(object sender, System.EventArgs e)
		{
			if (ShowURLs.SelectedIndex == -1)

			{
				await DisplayAlert("Error", "You Need to Pick A Link", "Ok");
			}

			else 
			{
				_urls = await _connection.Table<WebURL>().ToListAsync();
				var selectedItems = ShowURLs.SelectedIndex;
				var selectedClass = _urls[ShowURLs.SelectedIndex];
				await Navigation.PushModalAsync(new EditURLPage(selectedClass, selectedItems));
			}


		}

		async void RemoveURL(object sender, System.EventArgs e)
		{
            if (ShowURLs.SelectedIndex == -1)

			{
				await DisplayAlert("Error", "You Need to Pick A Link", "Ok");
			}

			else
			{
				_urls = await _connection.Table<WebURL>().ToListAsync();
				var selectedItem = _urls[ShowURLs.SelectedIndex];
				await _connection.DeleteAsync(selectedItem);

				ReloadPicker();
			}


        }
	}
}