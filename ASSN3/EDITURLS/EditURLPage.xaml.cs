﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using SetupSQLite;
using SQLite;
using Xamarin.Forms;

namespace ASSN3
{
	public partial class EditURLPage : ContentPage
	{
        private SQLiteAsyncConnection _connection;
        private ObservableCollection<WebURL> _url;
        private int selectedNumber;

        public EditURLPage(WebURL selectedClass, int selection)
		{
            InitializeComponent();
			_connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            BindingContext = selectedClass;
            selectedNumber = selection;
        }

		async void EditURL(object sender, EventArgs e)
		{
            var url = await _connection.Table<WebURL>().ToListAsync();
            _url = new ObservableCollection<WebURL>(url);

            var urlBlock = _url[selectedNumber];
            urlBlock.Title = TitleNew.Text;
            urlBlock.URL = URLNew.Text;
            urlBlock.ImageShow = ImageNew.Text;

            await _connection.UpdateAsync(urlBlock);
        }

		async void GoBack(object sender, EventArgs e)
		{
			await Navigation.PopModalAsync();
		}
	}
}